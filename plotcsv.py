import sys
sys.path.append("C:\Altium\standard-files\scripts")
from csvscope import csvscope
import os

def listar_arquivos_csv():
	# Obtém a lista de todos os arquivos no diretório atual.
	arquivos = os.listdir()

	# Filtra a lista para apenas arquivos .csv.
	arquivos_csv = [arquivo for arquivo in arquivos if arquivo.lower().endswith(".csv")]

	return arquivos_csv

def plota(f):
	CS = csvscope(f) # cria o objeto
	CS.format(f=f,n='ch1')
	CS.format(f=f,n='ch2',c=2)
	CS.plot()	# imprime o gráfico


if __name__ == "__main__":
	arquivos_csv = listar_arquivos_csv()
	for arquivo in arquivos_csv:
		print(arquivo)
		plota(arquivo)

input('pressione ENTER para continuar')