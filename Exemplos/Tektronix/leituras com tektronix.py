import sys
import copy
sys.path.append("C:\Altium\standard-files\scripts")
from csvscope import csvscope


c = {
	'cutoff in': -1e-3,
	'cutoff out': 35e-3,
}

VCC = copy.deepcopy(c)
VCC['findY'] = [7]

Titulo = 'Short Interrupt [30ms]'
CS = csvscope(Titulo) # cria o objeto
CS.format(f='F0007CH3',n='+VIN.CMC',config=c)
CS.format(f='F0007CH1',n='+VIN.TVS',config=c)
CS.format(f='F0007CH2',n='+VCC',config=VCC)
CS.format(f='F0007CH4',n='+5V',config=c)
CS.plot()

