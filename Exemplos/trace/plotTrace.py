import sys
import copy
sys.path.append("C:\Altium\standard-files\scripts")
from csvscope import csvscope

c = {
	'label x':'Tempo[s]',
}

Titulo = 'Analógicos'
CS = csvscope(Titulo) # cria o objeto
for i in range(0,8):
	CS.format(f='Trace',c=i+1,config = c)
CS.plot()


Titulo = 'Digitais'
CS = csvscope(Titulo) # cria o objeto
for i in range(8,16):
	CS.format(f='Trace',c=i+1,config = c)
CS.plot()


input()