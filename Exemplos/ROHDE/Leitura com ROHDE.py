import sys
import copy
sys.path.append("C:\Altium\standard-files\scripts")
from csvscope import csvscope
from pprint import pprint

c = {
	'findT':[0], # busca uma valor de tempo
	'findY':[2], # busca uma valor de tensão
	'note': ['transition'], # adiciona uma anotação de transição
	'label x':'Tempo[ns]', # define a notação de tempo específica
	}

produto='XP-perepepe'
sinal= 'UART RX'
ch2= 'UART.MCUB.RXD'
ch1= 'LPUART1.RX'
Titulo = sinal+' Fall Time ['+produto+']'
CS = csvscope(Titulo) # cria o objeto
CS.format(f='TRC285',c=2,n=ch1,config=c) # adiciona o primeiro sinal nele
CS.format(f='TRC285',n=ch2,config=c) # adiciona o segundo sinal nele
CS.drawDelay(ch2,'Tfall',ch1,'Trise') # adiciona uma anotação de delay entre s dois sinais
#CS.setAnotationDir('Delay','S')
CS.plot()	# imprime o gráfico


# em 24V
Titulo = 'Short Interrupt Máximo em 24V'
c = {
	'cutoff in': -1e-3, #corta tudo que estiver antes do tempo determinado
	'cutoff out': 120e-3,#corta tudo que estiver depois do tempo determinado
	'loc_legend': 'lower left' #posiciona a legenda
}

VCC = copy.deepcopy(c) #cria uma configuração especial
VCC['findY'] = [9,7] #adiciona 2 valores de tensão a serem buscados por quem usar essa configuração
queda3V = copy.deepcopy(c) #cria uma configuração especial
queda3V['findY'] = [3] #adiciona 1 valor de tensão a ser buscado por quem usar essa configuração
queda3V['label y'] = 'Tensão 2'

queda5V = copy.deepcopy(c)
queda5V['findT'] = [46.16e-3,51.41e-3] #adiciona 2 valores de tempo a serem buscados por quem usar essa configuração


CS = csvscope(Titulo) # cria o objeto
CS.format(f='TRC94',n='+VCC.TVS',config=c) # adiciona o primeiro sinal nele
CS.format(f='TRC95',n='+VCC',config=VCC) # adiciona o segundo sinal nele
CS.format(f='TRC96',n='+5V',config=c) # adiciona o terceiro sinal nele
CS.format(f='TRC97',n='+3V3',config=queda3V) # adiciona o quarto sinal nele
CS.formatLinhaV(x=0)
CS.formatLinhaH(y=0)
CS.plot()	# imprime o gráfico
