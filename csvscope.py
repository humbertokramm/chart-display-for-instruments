import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import csv
from copy import copy,deepcopy
from scipy.signal import welch
from scipy import signal
import os
import datetime
from pprint import pprint

EngNotation ={
	'Y':	1e24 ,	'Z':	1e21 ,	'E':	1e18 ,	'P':	1e15 ,	'T':	1e12 ,	'G':	1e9  ,
	'M':	1e6  ,	'k':	1e3  ,	'h':	1e2  ,	'da':	1e1  ,	'' :	1e0  ,	'd':	1e-1 ,
	'c':	1e-2 ,	'm':	1e-3 ,	'u':	1e-6 ,	'µ':	1e-6 ,	'n':	1e-9 ,	'p':	1e-12,
	'f':	1e-15,	'a':	1e-18,	'z':	1e-21,	'y':	1e-24,
	#extras
	#'cm^4':	1e-8,	'cm^3':	1e-6,	'cm^2':	1e-4,	'cm³':	1e-6,	'cm²':	1e-4,	'mm^4':	1e-12,
	#'mm^3':	1e-9,	'mm^2':	1e-6,	'mm³':	1e-9,	'mm²':	1e-6,	'%':  	1e-2
}
Symbol = ['V','W','A','Ω','s','Hz']

class csvscope:
	def __init__(self, title='Minhas Leituras',path = ''):
		self.reads = []
		self.title = title
		self.indexNote = 0
		self.path = path
		#self.ySeries = []
		self.yDf = pd.DataFrame(columns=['label','xMin','yMin','xMax','yMax','xAr','yAr','draw'])
		
	def __str__(self):
		return self.title

	def getEng(self, nota,s=False):
		i = nota.find('[')+1
		o = nota.find(']')
		if i>o:
			if s: return ''
			return 1
		nota = nota[i:o]
		qt = ''
		for q in Symbol:
			if nota.find(q)>-1:
				qt = q
			nota = nota.replace(q,'')
		if s == 'symbol': return qt
		if s == True: return nota+qt
		return EngNotation[nota]

	def getEngSTR(self,number):
		if number == 0: return '0 '
		exponent = int(np.floor(np.log10(abs(number))))
		exponent = (exponent // 3) * 3
		unit = {
			-30: 'q',-27: 'r',-24: 'y',-21: 'z',-18: 'a',-15: 'f',-12: 'p',-9: 'n',-6: 'µ',-3: 'm',
			0: '', 3: 'k', 6: 'M', 9: 'G', 12: 'T', 15: 'P', 18: 'E', 21: 'Z', 24: 'Y', 27: 'R', 30: 'Q'
		}
		eng_notation_string = "{:.2f} {}".format(number / 10 ** exponent, unit.get(exponent, ''))
		return eng_notation_string

	def getValue(self,number,s,axe):
		if axe == 'x':
			return self.getEngSTR(number*s['engNoteX'])+s['symbolX']
		if axe == 'f':
			return self.getEngSTR(number/s['engNoteX'])+'Hz'
		if axe == 'bps':
			return self.getEngSTR(number/s['engNoteX'])+'bps'
		if axe == 'y':
			return self.getEngSTR(number*s['engNoteY'])+s['symbolY']
		return 'ERROR: getValue(axe = '+axe+')' 

	def filtro(self,name='ch1',fc=1e3,ordem=4,overwrite=False):
		try:
			i= self.getOrder().index(name)
		except:
			print('não foi encontrada uma lista com o nome '+str(name))
			return
		T=self.reads[i]['x'].iat[1]-self.reads[i]['x'].iat[0]
		fs = 1/(T*self.reads[i]['engNoteX'])
		# filtro passa-baixas Butterworth
		b, a = signal.butter(ordem, fc/(fs/2), btype='low')
		# Aplicar o filtro ao sinal
		sinal_filtrado = signal.lfilter(b, a, self.reads[i]['y'])
		sinal_filtrado=pd.DataFrame(sinal_filtrado)
		if overwrite: self.reads[i]['y'] = sinal_filtrado
		return self.reads[i]['x']*self.reads[i]['engNoteX'],sinal_filtrado*self.reads[i]['engNoteY']

	def filtroInterno(self,df,filtro=[1e3,4]):
		fc = filtro[0]
		ordem = filtro[1]
		T=df['x'].iat[1]-df['x'].iat[0]
		fs = 1/(T*df['engNoteX'])
		# filtro passa-baixas Butterworth
		b, a = signal.butter(ordem, fc/(fs/2), btype='low')
		# Aplicar o filtro ao sinal
		sinal_filtrado = signal.lfilter(b, a, df['y'])
		sinal_filtrado=pd.DataFrame(sinal_filtrado)
		df['y'] = sinal_filtrado[sinal_filtrado.columns[0]]
		df['x'] = df['x'].reset_index(drop = True)

	def detectBrandFile(self, f):
		brand = ''
		try:
			with open(f, 'r') as file:
				firstLines = [next(file) for _ in range(3)]
			if len(firstLines)==3:
				if 'in s,CH' in firstLines[0]:
					if ' in V' in firstLines[0]:
						brand  = 'ROHDE'
				if '[key]; [value]' in firstLines[0]:
					if 'Version;' in firstLines[1]:
						if 'Name; Application.Trace' in firstLines[2]:
							brand  = 'Master Tool'
				if 'Record Length' in firstLines[0]:
					if 'Sample Interval' in firstLines[1]:
						if 'Trigger Point' in firstLines[2]:
							brand  = 'Tektronix'
		except FileNotFoundError:
			print("Arquivo não encontrado.")
		except Exception as e:
			print( f"Ocorreu um erro: {e}")
		return brand

	def MtoolCSV(self,f):
		with open(f, newline='') as csvfile:
			leitor_csv = csv.reader(csvfile, delimiter=';')
			data = {}
			timeLabel = 'in s'
			for linha in leitor_csv:
				index = linha[0].find('.')
				if linha[0][index+1:] == 'Variable':
					label = linha[1]
					data[label] = []
					data[timeLabel] = []
				if linha[0] == '':
					data[timeLabel].append(linha[1])
					data[label].append(linha[2])
			df = pd.DataFrame(data)
			df = df.sort_index(axis=1)
			df.insert(0, timeLabel, df.pop(timeLabel))
			df[timeLabel] = pd.to_numeric(df[timeLabel])
			df[timeLabel] = df[timeLabel] / 1000
		return df

	def TektronixCSV(self,f):
		df = pd.read_csv(f, header=None)
		labely = df[1][6]
		df = df.drop(df.columns[[0, 1, 2,5]], axis=1)
		df.columns = ['in s',labely]
		return df

	def readFile(self, f,data):
		i = f.lower().find('.csv')
		if i > 0:
			f = f[:i]+'.csv'
		else:
			f = f+'.csv'
		
		brd = self.detectBrandFile(f)
		info = os.stat(f).st_ctime
		info = datetime.datetime.fromtimestamp(info)
		info = str(info)[:16]

		if brd == 'Tektronix':
			df = self.TektronixCSV(f)
		elif brd == 'Master Tool':
			df = self.MtoolCSV(f)
		else: df = pd.read_csv(f)
		data['data'] = brd +' - '+ info
		return df

	def manualTable(self,x,y,dados):
		table = [x,y]
		df = pd.DataFrame(table)
		df = df.transpose()
		df.columns = ['in s', 'CH1 in V']
		dados['data'] = 'None'
		return df

	def formatLinhaH(self, y,n='serie',color=False,config=[]):
		dados = self.setData(n,config,'Line H')
		if color: dados['color']=color
		#lx=config['label x'] if 'label x' in config else 'Tempo[ms]'
		#ly=config['label y'] if 'label y' in config else 'Tensão[V]'
		#pln=config['plane'] if 'plane' in config else 1

		if not isinstance(y, (int, float)):
			print('ERROR formatLinhaH: Entre com um número válido em y')
			return 'nan'

		table = [[None,None],[y,y]]
		df = pd.DataFrame(table)
		df = df.transpose()
		df.columns = ['in s', 'CH1 in V']
		dados['data'] = 'None'
		
		# Manipula as escala
		self.handleScales(dados,df)
		self.areaGraf4(dados)
		self.reads.append(dados)
		return dados

	def formatLinhaV(self, x,n='serie',color=False,config=[]):
		dados = self.setData(n,config,'Line V')
		if color: dados['color']=color

		if not isinstance(x, (int, float)):
			print('ERROR formatLinhaV: Entre com um número válido em x')
			return 'nan'

		table = [[x,x],[None,None]]
		df = pd.DataFrame(table)
		df = df.transpose()
		df.columns = ['in s', 'CH1 in V']
		dados['data'] = 'None'
		
		# Manipula as escala
		self.handleScales(dados,df)
		self.areaGraf4(dados)
		self.reads.append(dados)
		return dados

	def getOrder(self):
		list = []
		for n in self.reads:
			list.append(n['name'])
		return list

	def setOrder(self,names='nan',index='nan'):
		temp = deepcopy(self.reads)
		if type(names) == type([]):
			self.reads = []
			for n in names:
				v,_= self.findkey(temp,n)
				self.reads.append(v)
			return True
		else: return False

	def setData(self, n,config,modo):
		lx=config['label x'] if 'label x' in config else 'Tempo[ms]'
		ly=config['label y'] if 'label y' in config else 'Tensão[V]'
		brd=config['brand'] if 'brand' in config else 'ROHDE'
		pln=config['plane'] if 'plane' in config else 1
		ot=config['offset time'] if 'offset time' in config else 0 
		dt=config['std duty'] if 'std duty' in config else [0.1,0.9]
		
		dados={
			'labelx':lx,
			'labely':ly,
			'name': n,
			'plane':pln,
			'engNoteX':self.getEng(lx),
			'engNoteY':self.getEng(ly),
			'engNoteXstr':self.getEng(lx,'str'),
			'engNoteYstr':self.getEng(ly,'str'),
			'symbolX':self.getEng(lx,'symbol'),
			'symbolY':self.getEng(ly,'symbol'),
			'type': modo,
			#'brand':brd,
			'offset time':ot,
			'std duty':dt,
			#'draw':[],
		}
		if 'note' in config: dados['note'] = config['note']
		if 'findY' in config: dados['findY'] = config['findY']
		if 'findT' in config: dados['findT'] = config['findT']
		if 'loc_legend' in config: dados['loc_legend'] = config['loc_legend']
		if 'loc_legend2' in config: dados['loc_legend2'] = config['loc_legend2']
		if dados['labely'] not in self.yDf['label'].values:
			self.yDf.loc[len(self.yDf), ['label']] = dados['labely']
		#pprint(dados)
		return dados

	def handleScales(self, d,df,o=0,g=1,c=1):
		lx = d['labelx']
		ly = d['labely']
		ot = d['offset time']
		d['x'] = df[df.columns[0]].astype(float)*(1/self.getEng(lx))+ot*(1/self.getEng(lx))
		d['y'] = df[df.columns[c]].astype(float)*g*(1/self.getEng(ly))+o*(1/self.getEng(ly))

	def handleCuts(self,df,config):
		coi=config['cutoff in'] if 'cutoff in' in config else 'nan'
		coo=config['cutoff out'] if 'cutoff out' in config else 'nan'
		
		# selecting rows based on condition
		if not coi =='nan': df = df.loc[df['in s'] >= coi]
		if not coo =='nan': df = df.loc[df['in s'] <= coo]
		return df

	def format(self, f='TRC01',g=1,o=0,c=1,x=[],y=[],n='nan',color=False,config=[],filtro=0):
		
		# Carrega as configurações
		dados = self.setData(n,config,'signal')
		if color: dados['color']=color
		
		# carrega o dataframe
		if len(x)==0: df = self.readFile(f,dados)
		else: df = self.manualTable(x,y,dados)
		
		# verifica o tamanho
		if(c+1 > len(df.columns)):
			print('ERROR format: Sua planilha de dados possui menos colunas do que o requisitado')
			print(df)
			return 'nan'
		# define o nome da série se vier em branco
		if n == 'nan':
			dados['name'] = df.columns[c]
		# Manipula os cortes
		df = self.handleCuts(df,config)
		if type(df) == type('nan'): return 'nan'
		
		# Manipula as escala
		self.handleScales(dados,df,o,g,c)
		self.areaGraf4(dados)
		if type(filtro) == type([]):
			self.filtroInterno(dados,filtro)
		self.getAnotations(dados)
		self.reads.append(dados)
		return dados

	def getAnotations(self, dados):
		for busca in ['note','findY','findT']:
				if busca in dados:
					for n in dados[busca]:
						self.anotation(n,dados,busca)

	def findkey(self,list,value,key='name'):
		for i,dictio in enumerate(list):
			if key in dictio:
				if dictio[key] == value:
					return dictio,i
		return None

	def findkey2(self,col,value,n):
		index = -1
		for row in col:
			index += 1
			for i in row:
				if i[4]==value:
					if i[1][:len(n)] == n:
						return i[0][0],index
		return None,None

	def drawDelay(self, s1,n1,s2,n2,u='s'):
		cord1,indexLabel = self.findkey2(self.yDf['draw'],s1,n1)
		cord2,indexLabel = self.findkey2(self.yDf['draw'],s2,n2)
		s_,_ = self.findkey(self.reads,s1)

		if cord1 == None:
			print(n1+' não existe em '+s1)
			return
		if cord2 == None:
			print(n2+' não existe em '+s1)
			return
		if cord1[1] > cord2[1]:
			y = cord1[1]
			x = cord2[0]
		else:
			y = cord2[1]
			x = cord1[0]
		cord=[[cord1[0],y],[cord2[0],y]]
		meioX = (cord2[0]-cord1[0])/2+cord1[0]
		meio = [[meioX,y],[None,None]]
		text=''
		style = '|-|'
		dir = 'delay'
		self.yDf['draw'][indexLabel].append([cord,text,style,dir,n1,':'])
		
		cordBar=[[x,cord1[1]],[x,cord2[1]]]
		style = '-'
		self.yDf['draw'][indexLabel].append([cordBar,text,style,dir,n1,':'])
		
		dt = abs(cord1[0]-cord2[0])
		if u == 's': text='Delay = '+self.getValue(dt,s_,'x')
		if u == 'Hz': text='Freq = '+self.getValue(1/dt,s_,'f')
		if u == 'bps': text='bit rate = '+self.getValue(1/dt,s_,'bps')
		style = '-'
		dir = 'NE'
		xo=cord1[0] if cord1[0] < cord2[0] else cord2[0]
		cordT = [[xo+dt/2,y],[None,None]]
		self.yDf['draw'][indexLabel].append([meio,text,style,dir,n1])

	def findNote(self,list, value,key='name'):
		for i in range(len(list)):
			if key in list[i]:
				for point in range(len(list[i][key])):
					if list[i][key][point][1][:len(value)] == value:
						return i,point
		return None,None

	def interpolateDF(self,df,sampleTarget=1000):
		# Verificar o número de amostras no DataFrame atual
		lenSample = len(df)
		temp=df.tolist()
		# Se o número atual de amostras for menor que o desejado, realizar a interpolação
		if lenSample < sampleTarget and lenSample > 0:
			newDF = []
			n=int(round(sampleTarget/lenSample,0))+1
			last=''
			for v in temp:
				if last == '': last = v
				else:
					for i in range(n):
						newDF.append(last+(v-last)*i/n)
					last = v
			newDF.append(last)
			df = pd.DataFrame(newDF)
			df = df[df.columns[0]]
		return df

	def setAnotationDir(self,n,dir):
		# Função para procurar e substituir com base no primeiro item da lista
		def setL(lista, string_verificacao, substituto):
			for i in range(len(lista)):
				if lista[i][1].startswith(string_verificacao):
					lista[i][3] = substituto
			return lista
		# Aplicar a função a cada linha do DataFrame
		self.yDf['draw'] = self.yDf.apply(lambda row: setL(row['draw'], n, dir), axis=1)

	def anotation(self, d,s,n):
		indexLabel = self.yDf.index[self.yDf['label'] == s['labely']].tolist()[0]
		name = s['name']
		x = self.interpolateDF(s['x'])
		y = self.interpolateDF(s['y'])
		if len(x) == 0: return print('ERROR ANOTATION: '+name+" has no signal")
		if n == 'note':
			if d == 'Vmáx':
				i = y.idxmax()
				y = y[i]
				x = x[i]
				cord=[[x,y],[None,None]]
				text=d+ ': '+self.getValue(y,s,'y')
				style = '->'
				dir = 'SE'
				self.yDf['draw'][indexLabel].append([cord,text,style,dir,name])
				return
				
			elif d == 'Vmin':
				i = y.idxmin()
				y = y[i]
				x = x[i]
				cord=[[x,y],[None,None]]
				text=d+ ': '+self.getValue(y,s,'y')
				style = '->'
				dir = 'NE'
				self.yDf['draw'][indexLabel].append([cord,text,style,dir,name])
				return
				
			elif d == 'RMS':
				rms = np.sqrt((y**2).mean())
				meio = int(len(y)/2)
				cord=[[x[meio],y[meio]],[None,None]]
				text=d+ ': '+self.getValue(rms,s,'y')
				style = '->'
				dir = 'NE'
				self.yDf['draw'][indexLabel].append([cord,text,style,dir,name])
				return
				
			elif d == 'ΔV':
				i1 = y.idxmin()
				y1 = y[i1]
				x1 = x[i1]
				i2 = y.idxmax()
				y2 = y[i2]
				x2 = x[i2]
				cord=[[x2,abs(y2-y1)/2+y1],[None,None]]
				text=d+ ': '+self.getValue(y2-y1,s,'y')
				style = '-'
				dir = 'NE'
				self.yDf['draw'][indexLabel].append([cord,text,style,dir,name])
				cord=[[x2,y1],[x2,y2]]
				text=''
				style = '|-|'
				dir = ''
				line = ':'
				self.yDf['draw'][indexLabel].append([cord,text,style,dir,name,':'])
				return
				
			elif d == 'transition' or d == 'transition in f':
				i1 = y.idxmax()
				i2 = y.idxmin()
				ymax = y[i1]
				ymin = y[i2]
				dV = ymax - ymin
				dy = dV*max(s['std duty'])+ymin
				i = (y - dy).abs().idxmin()
				x1 = x[i]
				y1 = y[i]
				dy = dV*min(s['std duty'])+ymin
				i = (y - dy).abs().idxmin()
				x2 = x[i]
				y2 = y[i]
				dt = abs(x2-x1)
				cord=[[x1,y1],[x2,y2]]
				text=''
				style = '|-|'
				dir = ''
				self.yDf['draw'][indexLabel].append([cord,text,style,dir,name,':'])
				if(i1>i2):
					d = 'Trise'
					cord=[[x1,y1],[None,None]]
					dir = 'SE'
				else:
					d = 'Tfall'
					cord=[[x2,y2],[None,None]]
					dir = 'NE'
				text=d+ ': '+self.getValue(dt,s,'x')
				if d == 'transition in f':
					text=d+ ': '+self.getValue(1/dt,s,'f')
				style = '->'
				self.yDf['draw'][indexLabel].append([cord,text,style,dir,name])
				return
		elif n == 'findY':
			d /= s['engNoteY']
			i = (y - d).abs().idxmin()
			y = y[i]
			x = x[i]
			if y/d > 1.05: return
			if d/y > 1.05: return
			
			cord=[[x,y],[None,None]]
			self.indexNote += 1
			id = 'p'+str(self.indexNote)
			text=id+' ('+self.getValue(x,s,'x')+' , '+self.getValue(y,s,'y')+')'
			style = '->'
			dir = 'NE'
			self.yDf['draw'][indexLabel].append([cord,text,style,dir,name])
			return
		elif n == 'findT':
			d /= s['engNoteX']
			i = (x - d).abs().idxmin()
			y = y[i]
			x = x[i]
			if d/x < 0.95: return
			if d/x > 1.05: return
			self.indexNote += 1
			id = 'p'+str(self.indexNote)
			cord=[[x,y],[None,None]]
			text=id+' ('+self.getValue(x,s,'x')+' , '+self.getValue(y,s,'y')+')'
			style = '->'
			dir = 'NE'
			self.yDf['draw'][indexLabel].append([cord,text,style,dir,name])
			return
		return

	def areaGraf(self, serie,area=None):
		min = 0
		max = 1
		x = 0
		y = 1

		if area == None:
			area =  [[serie['x'].min(),serie['y'].min()], [serie['x'].max(),serie['y'].max()],[2,1]]
			return area

		if area[min][y] >= serie['y'].min(): area[min][y] = serie['y'].min()
		if area[min][x] >= serie['x'].min(): area[min][x] = serie['x'].min()
		if area[max][y] <= serie['y'].max(): area[max][y] = serie['y'].max()
		if area[max][x] <= serie['x'].max(): area[max][x] = serie['x'].max()
		return area

	def areaGraf4(self, serie):
		# Obter o índice da linha que contém o valor "Bob" na coluna "Nome"
		i = self.yDf.index[self.yDf['label'] == serie['labely']].tolist()[0]
		
		if pd.isnull(self.yDf['xMax'][i]):
			self.yDf['xMin'][i] =  serie['x'].min()
			self.yDf['xMax'][i] =  serie['x'].max()
			self.yDf['yMin'][i] =  serie['y'].min()
			self.yDf['yMax'][i] =  serie['y'].max()
			self.yDf['draw'][i] = []
		else:
			if self.yDf['xMin'][i] >= serie['x'].min(): self.yDf['xMin'][i] = serie['x'].min()
			if self.yDf['xMax'][i] <= serie['x'].max(): self.yDf['xMax'][i] = serie['x'].max()
			if self.yDf['yMin'][i] >= serie['y'].min(): self.yDf['yMin'][i] = serie['y'].min()
			if self.yDf['yMax'][i] <= serie['y'].max(): self.yDf['yMax'][i] = serie['y'].max()
		
		self.yDf['xAr'][i] =  self.yDf['xMax'][i]-self.yDf['xMin'][i]
		self.yDf['yAr'][i] =  self.yDf['yMax'][i]-self.yDf['yMin'][i]
		return 

	def plotNotes(self,f,ax,i):#,notes):
		factor= 0.05
		figsize = f.get_size_inches()
		rate= figsize[1]/figsize[0]
		deltax = self.yDf['xAr'][i]*factor*rate
		deltay = self.yDf['yAr'][i]*factor
		#for note in notes:
		#print('plotNotes()')
		#pprint(self.yDf['draw'])
		for note in self.yDf['draw'][i]:
			#print('for note in self.yDf')
			#pprint(note)
			a=self.arrow(note,[deltax,deltay])
			ax.annotate(a['txt'], xy=a['xy'],xytext=a['xytext'],ha=a['ha'],va=a['va'],arrowprops=a['props'])

	def arrow(self, n,d=0):
		#print('arrow()')
		#pprint(n)
		xp = n[0][0][0]
		yp = n[0][0][1]
		#print(xp,yp)
		o = n[3]
		linestyle = '-'
		va = 'center'
		ha = 'center'
		if len(n)>5: linestyle=n[5]
		if   o == 'NE':	xo = xp+d[0];	yo = yp+d[1];		ha = 'left';	va = 'bottom'
		elif o == 'N':	xo = xp;			yo = yp+d[1]*2;	ha = 'center';va = 'bottom'
		elif o == 'NW':	xo = xp-d[0];	yo = yp+d[1];		ha = 'right';	va = 'bottom'
		elif o == 'W':	xo = xp-d[0];	yo = yp;				ha = 'right';	va = 'center'
		elif o == 'SW':	xo = xp-d[0];	yo = yp-d[1];		ha = 'right';	va = 'top'
		elif o == 'S':	xo = xp;			yo = yp-d[1]*2;	ha = 'center';va = 'top'
		elif o == 'SE':	xo = xp+d[0];	yo = yp-d[1];		ha = 'left';	va = 'top'
		elif o == 'E':	xo = xp+d[0];	yo = yp;				ha = 'left';	va = 'center'
		
		if n[0][1][0] == None: 
			n[0][1][0] = xo
			n[0][1][1] = yo
		
		result={
			'txt':n[1],
			'xy' :(n[0][0][0],n[0][0][1]),
			'xytext':(n[0][1][0],n[0][1][1]),
			'va':va,
			'ha':ha,
			'props': dict(arrowstyle=n[2], linestyle=linestyle)
		}
		#pprint(result)
		return result

	def rolling_rms(self, x, N):
		return (pd.DataFrame(abs(x)**2).rolling(N).mean()) **0.5

	def completeLines(self):
		# trata o sinal
		for serie in self.reads:
			indexLabel = self.yDf.index[self.yDf['label'] == serie['labely']].tolist()[0]
			if 'type' in serie:
				if serie['type'] == 'Line H':
					serie['x'] = [self.yDf['xMin'][indexLabel],self.yDf['xMax'][indexLabel]]
				if serie['type'] == 'Line V':
					serie['y'] = [self.yDf['yMin'][indexLabel],self.yDf['yMax'][indexLabel]]

	def seriePlot(self,ax,serie):
		if 'color' in serie:
			ax.plot(serie['x'], serie['y'], linewidth=2.0,label=serie['name'],color=serie['color'])
		else:
			ax.plot(serie['x'], serie['y'], linewidth=2.0,label=serie['name'])
		ax.set_ylabel(serie['labely'])
		ax.legend()

	def plot(self,t='nan',grid = True,size=(12, 6),out='png',path=''):
		if t == 'nan': t = self.title
		if path=='': path = self.path
		loc_legend = None
		loc_legend2 = None
		
		# verifica a consistência das séries
		if self.reads == []: return print('ERROR PLOT: Planilha incompleta')
		
		# Completa as linhas horizontais e verticais com os limites do gráfico
		self.completeLines()
		
		# Inicia a plotagem
		fig, ax = plt.subplots(figsize=size)#,axes_class=axisartist.Axes)
		ax.set_title(t)
		self.plotNotes(fig,ax,0)
		
		if len(self.yDf)>1:
			ax2 = ax.twinx() # Create another axes that shares the same x-axis as ax.
			self.plotNotes(fig,ax2,1)
		
		# Percorre plota as séries
		for serie in self.reads:
			indexLabel = self.yDf.index[self.yDf['label'] == serie['labely']].tolist()[0]
			#if serie['labely'] == self.ySeries[0]:
			if indexLabel == 0:
				self.seriePlot(ax,serie)
			else:
				self.seriePlot(ax2,serie)
			ax.set_xlabel(serie['labelx'])
			ax.grid(grid)
			
			if serie['data'] != 'None': data = serie['data']
			else: data = 'None'
			if 'loc_legend' in serie: loc_legend = serie['loc_legend']
			if 'loc_legend2' in serie: loc_legend2 = serie['loc_legend2']

		if loc_legend != None: ax.legend(loc=loc_legend)
		if loc_legend2 != None: ax2.legend(loc=loc_legend2)
		if data != 'None':
			ax.annotate(data,xy=(0.5,1e-2),xycoords='axes fraction', ha='center', fontsize=8)

		# Exibindo a figura
		if out == '':
			plt.savefig(t+'.png')
			plt.savefig(t+'.pdf')
		else: plt.savefig(path+t+'.'+out)
		plt.show(block=False)
		return

	def formatFFT(self,id=0,name=''):
		if name != '':
			_,id = self.findkey(self.reads,name)
		d = self.reads[id]
		
		x = d['x']*d['engNoteX']
		y = d['y']*d['engNoteY']
		
		fft = np.fft.fft(y)
		fft[0] = 0
		fftfreq = np.fft.fftfreq(len(y))*len(y)/(x.max()-x.min())

		a = []
		b = []
		for i in range(len(fftfreq)):
			if fftfreq[i] > 0:
				a.append(fftfreq[i])
				b.append(fft[i])
		self.reads[id]['fft']={'f':a,'A':np.abs(b)}

	def plotFFT(self, t='Minhas Leituras',grid = True,size=(12, 6),axe='linear',out='png',mark=1,path=''):
		for serie in self.reads:
			if t=='Minhas Leituras':
				t = self.title
			if path=='':
				path = self.path
			
			if 'fft' in serie:
				plt.subplots(figsize=size)
				plt.title(t)
				plt.xlabel("Domínio da Frequência [Hz]")
				plt.ylabel("Amplitude")

				a=serie['fft']['f']
				factor = 1
				if axe =='log': plt.semilogx()
				else:
					symbol = self.getEngSTR(max(a))[-1]
					factor = EngNotation[symbol]
					a = [float(i) / factor for i in serie['fft']['f']]
					plt.xlabel("Domínio da Frequência ["+symbol+"Hz]")
					
				x=pd.DataFrame(a)
				y=pd.DataFrame(serie['fft']['A'])
				
				# Obtendo o índice dos 10 maiores valores na coluna 'coluna2'
				largestValuesIndex = y[0].nlargest(mark).index
				j=0
				serie['draw']=[]
				for i in largestValuesIndex:
					j+=1
					y_ = y.loc[i,0]
					x_ = x.loc[i,0]
					
					cord=[[x_,y_],[None,None]]
					text='p'+str(j)+': '+self.getEngSTR(x_*factor)+'Hz'
					style = '-'
					dir = 'N'
					serie['draw'].append([cord,text,style,dir])
				
				plt.plot(x,y)
				plt.grid(grid,which="both")
				
				temp={'x':x,'y':y}
				area = self.areaGraf(temp)
				if serie['data'] != 'None':
					if axe =='linear': textX=(area[1][0]-area[0][0])/2+area[0][0]
					else: textX=np.sqrt(area[1][0])*np.sqrt(area[0][0])
					textY= area[0][1]-area[2][1]
					plt.text(textX,textY, serie['data'], ha='center', fontsize=8)
				
				for note in serie['draw']:
					#a=self.arrow(note,[area[2][1],area[2][1]])
					a=self.arrow(note,[0,0])
					#plt.annotate(a['txt'], xy=a['xy'],xytext=a['xytext'],arrowprops=dict(arrowstyle=a['arrowstyle'], linestyle=a['linestyle']))
					plt.annotate(a['txt'], xy=a['xy'],xytext=a['xytext'],ha=a['ha'],va=a['va'],arrowprops=a['props'])
				# Exibindo a figura
				if out == '':
					plt.savefig(t+'.png')
					plt.savefig(t+'.pdf')
				else: plt.savefig(path+t+'.'+out)
				plt.show(block=False)
		return
		
	def hold(self):
		input("Press ENTER key to continue...")